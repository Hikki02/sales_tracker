from drf_yasg import openapi

from apps.deals.serializers import TopCustomersSerializer


top_clients = {
    200: openapi.Response(
        description='Топ клиентов и покупок "gems"',
        schema=TopCustomersSerializer(many=True)
    ),
    500: openapi.Response(description='Internal server error'),
}