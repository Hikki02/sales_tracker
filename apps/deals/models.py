from django.db import models

from utils.base.base_model import BaseModel


class Deal(BaseModel):
    customer = models.CharField(max_length=100)
    item = models.CharField(max_length=100)
    total = models.DecimalField(max_digits=25, decimal_places=2)
    quantity = models.IntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return f"{self.customer} - {self.item}"
