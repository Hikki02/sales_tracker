from rest_framework import serializers
from .models import Deal


class DealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deal
        fields = (
            'customer',
            'item',
            'total',
            'quantity',
            'date',
        )


class DealUploadSerializer(serializers.Serializer):
    deals = serializers.FileField(help_text='CSV file')

    def validate_deals(self, value):
        if not value.name.endswith('.csv'):
            raise serializers.ValidationError("File is not in CSV format")
        return value


class TopCustomersSerializer(serializers.Serializer):
    username = serializers.CharField()
    spent_money = serializers.DecimalField(max_digits=10, decimal_places=2)
    gems = serializers.ListField(child=serializers.CharField())
