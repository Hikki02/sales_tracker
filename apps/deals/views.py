from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from apps.deals.serializers import DealUploadSerializer
from apps.deals.service import DealService
from utils.swagger.parameters import top_clients


@parser_classes((MultiPartParser,))
class DealUploadView(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = DealUploadSerializer
    service = DealService

    @swagger_auto_schema(
        request_body=DealUploadSerializer,
        responses={200: openapi.Response(description='файл был обработан без ошибок'),},
        operation_summary="Обработка CSV-файла с данными о заказах",
        operation_description="Метод для загрузки CSV-файла с данными о заказах."
    )
    def create(self, request, *args, **kwargs) -> Response:
        """
        Обрабатывает загрузку CSV-файла с данными о заказах.

        Args:
            request: Запрос, содержащий файл.

        Returns:
            Response: Ответ о результате обработки файла.
        """
        deals_file = request.FILES.get('deals')

        if not deals_file:
            return Response({'Status': 'Error', 'desc': 'No file received'}, status=status.HTTP_400_BAD_REQUEST)

        self.service.process_deals_csv(deals_file)
        return Response({'Status': 'OK', 'desc': 'file was processed successfully'}, status=status.HTTP_200_OK)

    @method_decorator(cache_page(60 * 15, key_prefix='top_clients'), name='dispatch')
    @swagger_auto_schema(
        responses=top_clients,
        operation_summary="Топ клиентов с информацией о покупках 'gems'",
        operation_description="Метод для получения списка топ клиентов и их покупок 'gems'."
    )
    def list(self, request) -> Response:
        """
        Возвращает топ клиентов с информацией о покупках "gems".

        Args:
            request: Запрос.

        Returns:
            Response: Ответ с информацией о топ клиентах и покупках "gems".
        """
        top_customers = self.service.get_top_customers_with_gems()
        response_data = {'response': top_customers}
        return Response(response_data, status=status.HTTP_200_OK)
