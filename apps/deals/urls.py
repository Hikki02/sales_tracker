from rest_framework import routers

from apps.deals.views import DealUploadView

router = routers.DefaultRouter()
router.register(r'deals', DealUploadView, basename='deal')
