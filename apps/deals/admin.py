from django.contrib import admin

from .models import Deal


@admin.register(Deal)
class DealsAdmin(admin.ModelAdmin):
    ...
