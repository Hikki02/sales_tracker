from django.contrib.auth import get_user_model
from django.contrib.auth.management.commands import createsuperuser
from django.core.management import CommandError


class Command(createsuperuser.Command):
    help = "Create a superuser, and allow password to be provided"

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument('--password', dest='password', default=None, help='Specifies the password for the superuser')

    def handle(self, *args, **options):
        user_model = get_user_model()
        password = options.get('password')
        username = options.get('username')
        email = options.get('email')
        database = options.get('database')

        if password and not username:
            raise CommandError("--username is required if specifying --password")

        if user_model._default_manager.filter(username=username).exists():
            self.stdout.write(self.style.WARNING(f"Superuser '{username}' already exists. Nothing to do."))
            return

        if password:
            user_model._default_manager.db_manager(database).create_superuser(
                username=username, email=email, password=password
            )
            self.stdout.write(self.style.SUCCESS(f"Superuser '{username}' created successfully!"))
        else:
            super().handle(*args, **options)
