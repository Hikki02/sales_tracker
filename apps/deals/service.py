from typing import List, Dict, Any

from django.core.exceptions import ValidationError
from django.db.models import Sum, Count

from utils.base.base_service import BaseService
from .models import Deal

import csv
from io import TextIOWrapper


class DealService(BaseService):
    model = Deal

    @classmethod
    def process_deals_csv(cls, deals_file) -> None:
        """
        Обрабатывает CSV-файл с данными о заказах и сохраняет данные в базе.

        Args:
            deals_file: CSV-файл с данными о заказах.

        Raises:
            ValidationError: Возникает при возникновении ошибок при обработке файла.
        """
        try:
            text_file = TextIOWrapper(deals_file.file, encoding='utf-8')
            csv_data = csv.DictReader(text_file)
            for row in csv_data:
                cls.create(
                    customer=row['customer'],
                    item=row['item'],
                    total=float(row['total']),
                    quantity=int(row['quantity']),
                    date=row['date']
                )
        except KeyError as e:
            raise ValidationError(f"Missing field in CSV: {e}")
        except (ValueError, TypeError) as e:
            raise ValidationError(f"Invalid data format in CSV: {e}")

    @classmethod
    def get_top_spending_customers(cls) -> List[Dict[str, Any]]:
        """
        Получает топ клиентов по общей сумме расходов.

        Returns:
            List[Dict[str, Any]]: Список словарей, каждый из которых содержит информацию о клиенте.
        """
        top_customers = cls.model.objects.values('customer').annotate(
            spent_money=Sum('total')
        ).order_by('-spent_money')[:5]

        return top_customers

    @classmethod
    def get_gems_for_top_customers(cls, top_customers: List[Dict[str, Any]]) -> Any:
        """
        Получает список товаров "gems" для топовых клиентов.

        Args:
            top_customers (List[Dict[str, Any]]): Список топовых клиентов.

        Returns:
            Any: Список товаров "gems".
        """
        customers_username = [customer['customer'] for customer in top_customers]

        gems = cls.model.objects.values('item').annotate(
            num_customers=Count('customer', distinct=True)
        ).filter(customer__in=customers_username, num_customers__gte=2)

        return gems

    @classmethod
    def get_customer_gems_list(
        cls, top_customers: List[Dict[str, Any]], gems: Any
    ) -> List[Dict[str, Any]]:
        """
        Создает список клиентов с информацией о покупках "gems".

        Args:
            top_customers (List[Dict[str, Any]]): Список топовых клиентов.
            gems (Any): Список товаров "gems".

        Returns:
            List[Dict[str, Any]]: Список словарей, каждый из которых содержит информацию о клиенте и его покупках "gems".
        """
        customers_with_gems = []

        for customer in top_customers:
            gems_list = cls.model.objects.filter(
                customer=customer['customer'],
                item__in=gems.values('item')
            ).values_list('item', flat=True).distinct()

            customers_with_gems.append({
                'username': customer['customer'],
                'spent_money': customer['spent_money'],
                'gems': list(gems_list)
            })

        return customers_with_gems

    @classmethod
    def get_top_customers_with_gems(cls) -> List[Dict[str, Any]]:
        """
        Получает топ клиентов с информацией о покупках "gems".

        Returns:
            List[Dict[str, Any]]: Список словарей с информацией о клиентах и их покупках "gems".
        """
        top_customers = cls.get_top_spending_customers()
        gems = cls.get_gems_for_top_customers(top_customers)
        customers_with_gems = cls.get_customer_gems_list(top_customers, gems)

        return customers_with_gems
