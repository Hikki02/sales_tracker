# Use an official Python runtime as a parent image
FROM python:3.11.0

# Set the working directory
WORKDIR /app/

# Install Poetry
RUN pip install poetry

# Copy only the files needed for installing dependencies
COPY pyproject.toml poetry.lock /app/

# Copy the entrypoint script into the container and give execute permissions
COPY ./entrypoint.sh /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

# Copy the current directory contents into the container at /app/
COPY . /app/

# Install project dependencies using Poetry
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

# Run the entrypoint script when the container launches
ENTRYPOINT ["/app/entrypoint.sh"]
